package view.util;

import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.scene.Node;
import javafx.util.Duration;

public class TransitionUtils {

    // ex = 400
    private static int duration = 100;

    private static boolean inFadeIn;
    private static boolean inFadeOut;

    public static void fadeIn(Node node) {
        if (inFadeIn) {
            return;
        }

        inFadeIn = true;

        node.setVisible(true);

        FadeTransition transition = new FadeTransition(Duration.millis(duration), node);
        transition.setFromValue(0);
        transition.setToValue(1);
        //transition.setInterpolator(Interpolator.EASE_BOTH);
        transition.play();

        transition.setOnFinished(e -> inFadeIn = false);
    }

    public static void fadeOut(Node node) {
        if (inFadeOut) {
            return;
        }

        inFadeOut = true;

        FadeTransition transition = new FadeTransition(Duration.millis(duration), node);
        transition.setFromValue(1);
        transition.setToValue(0);
        //transition.setInterpolator(Interpolator.EASE_BOTH);
        transition.play();

        transition.setOnFinished(e -> {
            node.setVisible(false);
            inFadeOut = false;
        });
    }

    public static void fadeInOut(Node in, Node out) {
        if (inFadeIn || inFadeOut) {
            return;
        }

        fadeIn(in);
        fadeOut(out);
    }
}