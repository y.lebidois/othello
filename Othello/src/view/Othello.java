package view;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.Window;
import model.Game;
import view.panes.GamePane;
import view.panes.HomePane;
import view.panes.PlayPane;
import view.panes.RulesPane;
import view.panes.event.ChangePaneEvent;
import view.util.TransitionUtils;

public class Othello extends Application {
    private static final int WIDTH = 1250;
    private static final int HEIGHT = 800;

    // Model
    private Game model;

    // Stage
    private Stage stage;

    // Main scene
    private Scene scene;
    private BorderPane root;

    // Home
    private Pane home;
    private Pane play;
    private Pane rules;

    // Game
    private Pane game;

    public static void main(String[] args) {
        launch(args);
    }

    private void createModel() {
        model = new Game(this);
    }

    private void createView() {
        stage.setTitle("Othello");
        stage.setResizable(false);
        stage.getIcons().add(new Image("/img/logo.png"));

        home = new HomePane(model);
        play = new PlayPane(model);
        rules = new RulesPane(model);
        game = new GamePane(model);
    }

    private void placeComponents() {
        root = new BorderPane();
        {
            StackPane stack = new StackPane();
            {
                stack.getChildren().addAll(home, play, rules);
                stack.setMinWidth(800);
            }
            root.setCenter(stack);

            VBox vbox = new VBox();
            {
                Label l1 = new Label("Projet");
                Label l2 = new Label("Théorie des jeux (2018)");
                Label l3 = new Label("Développeurs");
                Label l4 = new Label("LE CREURER Benjamin");
                Label l5 = new Label("LEBIDOIS Yohann");
                Label l6 = new Label("LEROUX Romuald");
                Label l7 = new Label("MAKHLOUF Bilal");

                Label nbsp = new Label();

                ImageView iv = new ImageView();
                iv.setImage(new Image("/img/logo_univ.png"));

                vbox.getChildren().add(l1);
                vbox.getChildren().add(l2);
                vbox.getChildren().add(l3);

                vbox.getChildren().add(l4);
                vbox.getChildren().add(l5);
                vbox.getChildren().add(l6);
                vbox.getChildren().add(l7);
                vbox.getChildren().add(nbsp);
                vbox.getChildren().add(iv);

                // Classe CSS
                vbox.getStyleClass().add("vbox");
                l1.getStyleClass().add("lbl-title");
                l3.getStyleClass().add("lbl-title");
                l2.getStyleClass().add("lbl-sub");
                l4.getStyleClass().add("lbl-sub");
                l5.getStyleClass().add("lbl-sub");
                l6.getStyleClass().add("lbl-sub");
                l7.getStyleClass().add("lbl-sub");
                nbsp.getStyleClass().add("nbsp-50");

                //vbox.setPrefWidth(400);
            }
            root.setRight(vbox);
        }

        scene = new Scene(root, WIDTH, HEIGHT);
        scene.getStylesheets().add("css/style.css");

        stage.setScene(scene);
    }

    private void createController() {
        stage.setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
        });

        home.addEventHandler(ChangePaneEvent.HOME_TO_PLAY, e -> TransitionUtils.fadeInOut(play, home));
        home.addEventHandler(ChangePaneEvent.HOME_TO_RULES, e -> TransitionUtils.fadeInOut(rules, home));

        play.addEventHandler(ChangePaneEvent.PLAY_TO_HOME, e -> TransitionUtils.fadeInOut(home, play));
        rules.addEventHandler(ChangePaneEvent.RULES_TO_HOME, e -> TransitionUtils.fadeInOut(home, rules));

        play.addEventHandler(ChangePaneEvent.PLAY_TO_GAME, e -> {
            scene.setRoot(game);
        });
    }

    public void switchSceneHome() {
        scene.setRoot(root);
    }

    @Override
    public void start(Stage primaryStage) {
        stage = primaryStage;

        createModel();
        createView();
        placeComponents();
        createController();

        stage.show();
    }
}