package view.panes;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.util.StringConverter;
import model.Game;
import model.player.PlayerType;
import model.player.pawn.PawnColor;
import model.player.Player;
import model.player.ai.Level;
import view.Logger;
import view.panes.event.ChangePaneEvent;

public class PlayPane extends AbstractOthelloPane {

    private ChoiceBox<PlayerType> player1Choice;
    private ChoiceBox<PlayerType> player2Choice;
    private Circle player1Pawn;
    private Circle player2Pawn;
    private Slider player1Difficulty;
    private Slider player2Difficulty;
    private Button startButton;
    private Button backButton;

    public PlayPane(Game model) {
        super(model);

        setVisible(false);
    }

    @Override
    protected void createView() {
        ObservableList<PlayerType> options = FXCollections.observableArrayList(PlayerType.values());

        player1Choice = new ChoiceBox(options);
        player1Choice.getSelectionModel().select(0);

        player2Choice = new ChoiceBox(options);
        player2Choice.getSelectionModel().select(1);

        player1Pawn = new Circle(25, Color.BLACK);
        player1Pawn.setStrokeWidth(3);
        player1Pawn.setStroke(Color.GRAY);

        player2Pawn = new Circle(25, Color.WHITE);
        player2Pawn.setStrokeWidth(3);
        player2Pawn.setStroke(Color.GRAY);

        player1Difficulty = createDifficultySlider();
        player1Difficulty.setDisable(true);

        player2Difficulty = createDifficultySlider();

        startButton = new Button("Démarrer");
        backButton = new Button("Retour");

        // Classes CSS
        startButton.getStyleClass().add("btn-green");
        backButton.getStyleClass().add("btn-back");
        player1Choice.getStyleClass().add("lst-play");
        player2Choice.getStyleClass().add("lst-play");
        player1Difficulty.getStyleClass().add("scroll-play");
        player2Difficulty.getStyleClass().add("scroll-play");
    }

    @Override
    protected void placeComponents() {
        BorderPane pane = new BorderPane();
        {
            Label t = new Label("Jouer");
            Label nbsp = new Label();

            // Class CSS
            t.getStyleClass().add("title");
            t.getStyleClass().add("title-play");

            nbsp.getStyleClass().add("nbsp-25");

            pane.setTop(t);

            VBox vbox = new VBox();
            {
                Label p1 = new Label("Joueur 1");

                /* Class CSS */
                p1.getStyleClass().add("lbl-player");

                vbox.getChildren().add(p1);

                HBox hbox = new HBox();
                {
                    FlowPane p = new FlowPane();
                    {
                        p.getChildren().add(player1Pawn);

                        // Class CSS
                        p.getStyleClass().add("mrg-choice");
                    }

                    hbox.getChildren().addAll(player1Choice, p);

                    // Class CSS
                    hbox.getStyleClass().add("pawn-play");
                }

                vbox.getChildren().addAll(hbox, player1Difficulty);

                vbox.getChildren().add(nbsp);

                Label p2 = new Label("Joueur 2");

                /* Class CSS */
                p2.getStyleClass().add("lbl-player");

                vbox.getChildren().add(p2);

                hbox = new HBox();
                {
                    FlowPane p = new FlowPane();
                    {
                        p.getChildren().add(player2Pawn);

                        // Class CSS
                        p.getStyleClass().add("mrg-choice");
                    }

                    hbox.getChildren().addAll(player2Choice, p);

                    // Class CSS
                    hbox.getStyleClass().add("pawn-play");
                }
                vbox.getChildren().addAll(hbox, player2Difficulty);

                // Class CSS
                vbox.getStyleClass().add("vbox-play");
            }
            pane.setCenter(vbox);

            HBox hBox = new HBox();
            {
                hBox.getChildren().addAll(backButton, startButton);

                // Class CSS
                hBox.getStyleClass().add("btn-play");
            }
            pane.setBottom(hBox);

            // Class CSS
            pane.getStyleClass().add("pane-play");
        }
        getChildren().add(pane);
    }

    @Override
    protected void createController() {
        player1Choice.setOnAction(e -> player1Difficulty.setDisable(player1Choice.getSelectionModel().getSelectedIndex() == 0));

        player2Choice.setOnAction(e -> player2Difficulty.setDisable(player2Choice.getSelectionModel().getSelectedIndex() == 0));

        player1Pawn.setOnMouseClicked(e -> swapPawn());
        player2Pawn.setOnMouseClicked(e -> swapPawn());

        player1Difficulty.valueProperty().addListener((obs, oldval, newVal) ->
                player1Difficulty.setValue(Math.round(newVal.doubleValue())));

        player2Difficulty.valueProperty().addListener((obs, oldval, newVal) ->
                player2Difficulty.setValue(Math.round(newVal.doubleValue())));

        startButton.setOnAction(e -> {
            PlayerType p1Type = player1Choice.getSelectionModel().getSelectedItem();
            Level p1Difficulty = Level.values()[(int)player1Difficulty.getValue() - 1];
            PawnColor p1Color = PawnColor.getPawnColorOf(player1Pawn.getFill());

            PlayerType p2Type = player2Choice.getSelectionModel().getSelectedItem();
            Level p2Difficulty = Level.values()[(int)player2Difficulty.getValue() - 1];
            PawnColor p2Color = PawnColor.getPawnColorOf(player2Pawn.getFill());

            Player p1 = model.generatePlayer(p1Type, p1Difficulty, p1Color);
            Player p2 = model.generatePlayer(p2Type, p2Difficulty, p2Color);

            model.startNewGame(p1, p2);
            Logger.getInstance().refresh();

            fireEvent(new ChangePaneEvent(ChangePaneEvent.PLAY_TO_GAME));
        });

        backButton.setOnAction(e -> {
            player1Choice.getSelectionModel().select(0);
            player2Choice.getSelectionModel().select(1);

            player1Pawn.setFill(Color.BLACK);
            player2Pawn.setFill(Color.WHITE);

            player1Difficulty.setValue(2);
            player2Difficulty.setValue(2);

            fireEvent(new ChangePaneEvent(ChangePaneEvent.PLAY_TO_HOME));
        });
    }

    private Slider createDifficultySlider() {
        Slider slider = new Slider();
        slider.setMin(1);
        slider.setMax(3);
        slider.setValue(2);
        slider.setShowTickLabels(true);
        slider.setBlockIncrement(1);

        slider.setLabelFormatter(new StringConverter<Double>() {
            @Override
            public String toString(Double n) {
                if (n < 2) return "Facile";
                if (n < 3) return "Moyen";

                return "Difficile";
            }

            @Override
            public Double fromString(String s) {
                switch (s) {
                    case "Facile":
                        return 1d;
                    case "Moyen":
                        return 2d;
                    default:
                        return 3d;
                }
            }
        });

        return slider;
    }

    private void swapPawn() {
        Paint p1 = player1Pawn.getFill();

        player1Pawn.setFill(p1 == Color.BLACK ? Color.WHITE : Color.BLACK);
        player2Pawn.setFill(p1 == Color.BLACK ? Color.BLACK : Color.WHITE);
    }
}