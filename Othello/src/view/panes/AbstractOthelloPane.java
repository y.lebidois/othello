package view.panes;

import javafx.scene.layout.Pane;
import javafx.scene.shape.Path;
import model.Game;

public abstract class AbstractOthelloPane extends Pane {

    protected Game model;

    AbstractOthelloPane(Game model) {
        this.model = model;

        createView();
        placeComponents();
        createController();
    }

    protected abstract void createView();
    protected abstract void placeComponents();
    protected abstract void createController();
}