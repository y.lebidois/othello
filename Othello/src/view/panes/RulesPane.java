package view.panes;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.text.TextAlignment;
import model.Game;
import view.panes.event.ChangePaneEvent;

public class RulesPane extends AbstractOthelloPane {

    private Button backButton;

    public RulesPane(Game model) {
        super(model);

        setVisible(false);
    }

    @Override
    protected void createView() {
        backButton = new Button("Retour");

        // Class CSS
        backButton.getStyleClass().add("btn-back");
    }

    @Override
    protected void placeComponents() {

        BorderPane pane = new BorderPane();
        {
            Label t = new Label("Règles");

            // Class CSS
            t.getStyleClass().add("title");
            t.getStyleClass().add("grid-rules");

            pane.setTop(t);

            GridPane grid = new GridPane();
            {
                String s1 = "Othello se joue à 2, sur un plateau unicolore de 64 cases (8 sur 8), avec des pions bicolores, " +
                        "noirs d'un côté et blancs de l'autre.";
                String s2 = "Le but du jeu est d'avoir plus de pions de sa couleur que l'adversaire à la fin de la partie, "
                        + "celle-ci s'achevant lorsque aucun des deux joueurs ne peut plus jouer de coup légal, "
                        + "généralement lorsque les 64 cases sont occupées.";
                String s3 = "Au début de la partie, la position de départ  est indiquée ci-contre. Les noirs commencent.";
                String s4 = "Chacun à son tour, les joueurs vont poser un pion de leur couleur sur une case vide, adjacente à un pion adverse."
                        + "Chaque pion posé doit obligatoirement encadrer un ou plusieurs pions adverses avec un autre pion de sa couleur, déjà placé";
                String s5 = "Il retourne alors le ou les pions adverse(s) qu'il vient d'encadrer. Les pions ne sont ni retirés de l'othellier, "
                        + " ni déplacés d'une case à l'autre.";
                String s6 = "On peut encadrer des pions adverses dans les huit directions et plusieurs pions peuvent être encadrés dans chaque direction.";

                HBox v1 = new HBox();
                HBox v2 = new HBox();

                // Premier bloc
                grid.add(v1, 0, 0, 1, 3);
                grid.add(createJustifiedLabel(s1), 1, 0, 4, 1);
                grid.add(createJustifiedLabel(s2), 1, 1, 4, 1);
                grid.add(createJustifiedLabel(s3), 1, 2, 4, 1);

                // Second bloc
                grid.add(v2, 3, 3, 1, 3);
                grid.add(createJustifiedLabel(s4), 0, 3, 3, 1);
                grid.add(createJustifiedLabel(s5), 0, 4, 3, 1);
                grid.add(createJustifiedLabel(s6), 0, 5, 3, 1);

                // Class CSS
                grid.getStyleClass().add("grid-rules");
                v1.getStyleClass().add("img-rules-1");
                v2.getStyleClass().add("img-rules-2");
            }
            pane.setCenter(grid);

            FlowPane p = new FlowPane();
            {
                p.getChildren().add(backButton);

                // Class CSS
                p.getStyleClass().add("btn-rules");
            }

            pane.setBottom(p);
        }


        getChildren().add(pane);
    }

    @Override
    protected void createController() {
        backButton.setOnAction(e -> fireEvent(new ChangePaneEvent(ChangePaneEvent.RULES_TO_HOME)));
    }

    private Label createJustifiedLabel(String text) {
        Label l = new Label(text);

        l.setTextAlignment(TextAlignment.JUSTIFY);
        l.setWrapText(true);
        l.setMaxWidth(600);

        return l;
    }
}