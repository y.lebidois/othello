package view.panes.event;

import javafx.event.Event;
import javafx.event.EventType;

public class ChangePaneEvent extends Event {

    public static final EventType<ChangePaneEvent> HOME_TO_PLAY = new EventType<>(Event.ANY, "HOME_TO_PLAY");
    public static final EventType<ChangePaneEvent> HOME_TO_RULES = new EventType<>(Event.ANY, "HOME_TO_RULES");
    public static final EventType<ChangePaneEvent> PLAY_TO_HOME = new EventType<>(Event.ANY, "PLAY_TO_HOME");
    public static final EventType<ChangePaneEvent> RULES_TO_HOME = new EventType<>(Event.ANY, "RULES_TO_HOME");
    public static final EventType<ChangePaneEvent> PLAY_TO_GAME = new EventType<>(Event.ANY, "PLAY_TO_GAME");

    public ChangePaneEvent(EventType<? extends Event> eventType) {
        super(eventType);
    }
}