package view.panes;

import javafx.application.Platform;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.TextFlow;
import model.Board;
import model.Game;
import model.GameModel;
import model.exception.InvalidMoveException;
import model.listener.GameModelListener;
import model.player.Player;
import model.util.Coordinate;
import view.Logger;
import view.util.TransitionUtils;

public class GamePane extends AbstractOthelloPane {

    private static final Image PLAY_IMAGE = new Image(GamePane.class.getClassLoader().getResourceAsStream("img/play.png"));
    private static final Image PLAY_DISABLED_IMAGE = new Image(GamePane.class.getClassLoader().getResourceAsStream("img/play_disabled.png"));
    private static final Image PAUSE_IMAGE = new Image(GamePane.class.getClassLoader().getResourceAsStream("img/pause.png"));
    private static final Image PAUSE_DISABLED_IMAGE = new Image(GamePane.class.getClassLoader().getResourceAsStream("img/pause_disabled.png"));
    private static final Image NEXT_IMAGE = new Image(GamePane.class.getClassLoader().getResourceAsStream("img/next.png"));
    private static final Image NEXT_DISABLED_IMAGE = new Image(GamePane.class.getClassLoader().getResourceAsStream("img/next_disabled.png"));
    private static final Image QUIT_IMAGE = new Image(GamePane.class.getClassLoader().getResourceAsStream("img/quit.png"));
    private static final Image QUIT_DISABLED_IMAGE = new Image(GamePane.class.getClassLoader().getResourceAsStream("img/quit_disabled.png"));

    private GameModel game;

    private Label player1Name;
    private Label player2Name;

    private Circle player1Pawn;
    private Circle player2Pawn;

    private Label player1Score;
    private Label player2Score;
    private Label round;
    private Label currentPlayer;

    private ImageView quitButton;
    private ImageView playButton;
    private ImageView nextButton;

    private Canvas canvas;

    private TextFlow textFlow;
    private EndGamePane endPane;
    private EchapGamePane echapPane;

    public GamePane(Game model) {
        super(model);
    }

    @Override
    protected void createView() {
        player1Name = new Label();
        player2Name = new Label();

        player1Score = new Label();
        player2Score = new Label();

        quitButton = new ImageView(QUIT_IMAGE);
        playButton = new ImageView(PAUSE_IMAGE);
        nextButton = new ImageView(NEXT_DISABLED_IMAGE);
        nextButton.setDisable(true);

        canvas = new Canvas(700, 700);

        round = new Label();
        currentPlayer = new Label();

        player1Pawn = new Circle(25, Color.BLACK);
        player1Pawn.setStrokeWidth(3);
        player1Pawn.setStroke(Color.GRAY);

        player2Pawn = new Circle(25, Color.WHITE);
        player2Pawn.setStrokeWidth(3);
        player2Pawn.setStroke(Color.GRAY);

        textFlow = Logger.getInstance().getTextFlow();

        // Class CSS
        round.getStyleClass().add("lbl-game");
        currentPlayer.getStyleClass().add("lbl-sub");
        currentPlayer.getStyleClass().add("lbl-italic");
        player1Score.getStyleClass().add("lbl-score");
        player2Score.getStyleClass().add("lbl-score");
        textFlow.getStyleClass().add("txt-log");
    }

    @Override
    protected void placeComponents() {
        BorderPane pane = new BorderPane();
        {
            pane.setMinSize(1260, 810);
            BorderPane p = new BorderPane();
            {
                HBox hbox = new HBox();
                {
                    VBox player1Box = new VBox();
                    {
                        StackPane stack = new StackPane();
                        {
                            stack.getChildren().addAll(player1Pawn, player1Score);
                        }
                        player1Box.getChildren().addAll(player1Name, stack);

                        // Class CSS
                        player1Box.getStyleClass().add("player-box-game");
                        player1Box.getStyleClass().add("mrg-right-75");
                    }

                    VBox player2Box = new VBox();
                    {
                        StackPane stack = new StackPane();
                        {
                            stack.getChildren().addAll(player2Pawn, player2Score);
                        }
                        player2Box.getChildren().addAll(player2Name, stack);

                        // Class CSS
                        player2Box.getStyleClass().add("player-box-game");
                        player2Box.getStyleClass().add("mrg-left-75");
                    }

                    hbox.getChildren().addAll(player1Box, player2Box);

                    // Class CSS
                    hbox.getStyleClass().add("box-game");
                }
                p.setMinWidth(800);
                p.setTop(hbox);
                p.setCenter(canvas);
            }
            pane.setCenter(p);

            p = new BorderPane();
            {
                VBox boxInfo = new VBox();
                {
                    boxInfo.getChildren().addAll(round, currentPlayer);

                    // Class CSS
                    boxInfo.getStyleClass().add("mrg-left-60");
                }
                p.setTop(boxInfo);

                FlowPane f = new FlowPane();
                {
                    ScrollPane sp = new ScrollPane(textFlow);

                    // Autoscroll
                    sp.vvalueProperty().bind(textFlow.heightProperty());
                    f.getChildren().add(sp);

                    // Class CSS
                    sp.getStyleClass().add("scroll-log");
                }
                p.setCenter(f);

                HBox hbox = new HBox();
                {
                    hbox.getChildren().addAll(quitButton, playButton, nextButton);

                    // Class CSS
                    hbox.getStyleClass().add("btn-game");
                    hbox.getStyleClass().add("bottom-box-game");
                }
                p.setBottom(hbox);

                // Class CSS
                p.getStyleClass().add("box-log");
                f.getStyleClass().add("area-log");
            }
            pane.setRight(p);
        }
        getChildren().add(pane);

    }

    @Override
    protected void createController() {

        model.addGameListener(() -> {
            game = model.getCurrentGame();

            endPane = new EndGamePane(model);
            echapPane = new EchapGamePane(model, this);

            getChildren().add(endPane);
            getChildren().add(echapPane);

            player1Name.setText(game.getPlayer1().toString());
            player2Name.setText(game.getPlayer2().toString());

            Color player1Color = game.getPlayer1().getColor().getColorValue();
            Color player2Color = game.getPlayer2().getColor().getColorValue();

            player1Pawn.setFill(player1Color);
            player2Pawn.setFill(player2Color);

            player1Score.setTextFill(player2Color);
            player2Score.setTextFill(player1Color);

            // Si un des joueurs est un humain
            if (!game.getPlayer1().isAI() || !game.getPlayer2().isAI()) {
                playButton.setDisable(true);
                playButton.setImage(PAUSE_DISABLED_IMAGE);
                disableNextButton(true);
            }

            update();

            game.addBoardListener(new GameModelListener() {
                @Override
                public void gameEnded() {
                    Player winner;

                    if (game.getPlayerScore(game.getPlayer1()) > game.getPlayerScore(game.getPlayer2())) {
                        winner = game.getPlayer1();
                    } else if (game.getPlayerScore(game.getPlayer1()) < game.getPlayerScore(game.getPlayer2())) {
                        winner = game.getPlayer2();
                    } else {
                        winner = null;
                    }

                    disableQuitButton(true);
                    disablePlayButton(true);
                    disableNextButton(true);
                    endPane.triggerWinner(winner);
                    Platform.runLater(() -> TransitionUtils.fadeIn(endPane));
                }

                @Override
                public void gameChanged() {
                    Platform.runLater(() -> {

                        update();
                    });
                }

                @Override
                public void gameStarted() {
                    disableQuitButton(false);
                    disablePlayButton(false);
                    if (game.isPause()) {
                        disableNextButton(false);
                    } else {
                        disableNextButton(true);
                    }
                    if (!game.getPlayer1().isAI() || !game.getPlayer2().isAI()) {
                        disablePlayButton(true);
                        disableNextButton(true);
                    }
                }
            });
        });

        playButton.setOnMouseClicked(e -> {
            playButton.setImage(game.isPause() ? PAUSE_IMAGE : PLAY_IMAGE);
            disableNextButton(game.isPause());

            if (game.isPause()) {
                game.start();
            } else {
                game.pause();
            }
        });

        quitButton.setOnMouseClicked(e -> {
            TransitionUtils.fadeIn(echapPane);
            disableQuitButton(true);
            disablePlayButton(true);
            disableNextButton(true);
        });


        nextButton.setOnMouseClicked(e -> {
            disableNextButton(true);

            game.next();
        });

        canvas.setOnMouseClicked(e -> {
            Player currentPlayer = game.getCurrentPlayer();
            if (currentPlayer == null || currentPlayer.isAI()) {
                return;
            }

            double fullSize = canvas.getWidth();
            double gridSize = fullSize - 50;
            double step = gridSize / Board.WIDTH;

            int x = (int) ((e.getX() - 25) / step);
            int y = (int) ((e.getY() - 25) / step);

            Coordinate c = new Coordinate(x, y);

            try {
                currentPlayer.playAt(c);
                Logger.getInstance().addLog(currentPlayer + " a joué en " + c);
            } catch (InvalidMoveException ime) {
                // rien
            }
        });
    }

    private void update() {
        if (game == null) {
            return;
        }

        if (game.isPause()) {
            disableNextButton(false);
        }

        // Mise à jour du canvas
        repaint();

        // Mise à jour des scores
        player1Score.setText(String.valueOf(game.getPlayerScore(game.getPlayer1())));
        player2Score.setText(String.valueOf(game.getPlayerScore(game.getPlayer2())));

        if (game.getPossibleMove(game.getCurrentPlayer()).size() == 0) {
            return;
        }

        // Mise à jour du nombre de tours
        round.setText("Tour n°" + game.getRound());
        currentPlayer.setText("C'est à " + game.getCurrentPlayer().toString() + " de jouer ...\n");

        Logger.getInstance().addSeparator(game.getRound());
    }

    private void repaint() {
        // Mise à jour de la grille
        GraphicsContext gc = canvas.getGraphicsContext2D();

        // Données de calcul
        double fullSize = canvas.getWidth();
        double gridSize = fullSize - 50;
        double step = gridSize / Board.WIDTH;
        double pawnSize = step * 0.8;
        double pawnShift = step / 2 + 25 - pawnSize / 2;
        double possibleMovePawnSize = step * 0.15;
        double possibleMovePawnShift = step / 2 + 25 - possibleMovePawnSize / 2;

        // Clear de la zone de dessin
        gc.clearRect(0, 0, fullSize, fullSize);

        gc.setStroke(Color.BLACK);
        gc.setLineWidth(2);

        // Premier cadre
        gc.setFill(Color.rgb(240, 170, 120));
        gc.fillRect(0, 0, fullSize, fullSize);
        gc.strokeRect(0, 0, fullSize, fullSize);

        // Second cadre
        gc.setFill(Color.rgb(220, 255, 220));
        gc.fillRect(25, 25, gridSize, gridSize);
        gc.strokeRect(25, 25, gridSize, gridSize);

        for (int i = 0; i < Board.WIDTH; i++) {
            gc.setFill(Color.BLACK);
            gc.setLineWidth(1);
            gc.setStroke(Color.grayRgb(100));

            gc.strokeLine(25, 25 + i * step, gridSize + 25, 25 + i * step);
            gc.strokeLine(i * step + 25, 25, +i * step + 25, gridSize + 25);

            gc.strokeText(String.valueOf((char) (64 + i + 1)), i * step + step / 2 + 23, 18);
            gc.strokeText(String.valueOf((char) (64 + i + 1)), i * step + step / 2 + 23, fullSize - 8);

            gc.strokeText(String.valueOf(i + 1), 8, i * step + step - 10);
            gc.strokeText(String.valueOf(i + 1), fullSize - 18, i * step + step - 10);

            for (int j = 0; j < Board.HEIGHT; j++) {
                Player player = game.getBoard().getPlayerAt(new Coordinate(i, j));

                if (player != null) {
                    gc.setLineWidth(3);
                    gc.setStroke(Color.GRAY);

                    gc.setFill(game.getBoard().getColorOf(player).getColorValue());
                    gc.fillOval(i * step + pawnShift, j * step + pawnShift, pawnSize, pawnSize);
                    gc.strokeOval(i * step + pawnShift, j * step + pawnShift, pawnSize, pawnSize);
                }
            }
        }

        gc.setFill(Color.ORANGERED);
        for (Coordinate coordinate : game.getPossibleMove(game.getCurrentPlayer())) {
            gc.fillOval(coordinate.getX() * step + possibleMovePawnShift, coordinate.getY() * step + possibleMovePawnShift,
                    possibleMovePawnSize, possibleMovePawnSize);
        }
    }

    public void disableNextButton(boolean disable) {
        nextButton.setDisable(disable);
        nextButton.setImage(disable ? NEXT_DISABLED_IMAGE : NEXT_IMAGE);
    }

    public void disableQuitButton(boolean disable) {
        quitButton.setDisable(disable);
        quitButton.setImage(disable ? QUIT_DISABLED_IMAGE : QUIT_IMAGE);
    }

    public void disablePlayButton(boolean disable) {
        playButton.setDisable(disable);
        if (game.isPause()) {
            playButton.setImage(disable ? PLAY_DISABLED_IMAGE : PLAY_IMAGE);
        } else {
            playButton.setImage(disable ? PAUSE_DISABLED_IMAGE : PAUSE_IMAGE);
        }
    }

}