package view.panes;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import model.Game;
import view.panes.event.ChangePaneEvent;

public class HomePane extends AbstractOthelloPane {

    private Button playButton;
    private Button rulesButton;
    private Button quitButton;

    public HomePane(Game model) {
        super(model);
    }

    @Override
    protected void createView() {
        playButton = new Button("Jouer");
        rulesButton = new Button("Règles");
        quitButton = new Button("Quitter");

        playButton.getStyleClass().add("btn-black");
        rulesButton.getStyleClass().add("btn-black");
        quitButton.getStyleClass().add("btn-red");
    }

    @Override
    protected void placeComponents() {
        GridPane home = new GridPane();
        {
            Label t = new Label("Othello");
            Label nbsp = new Label();

            home.add(t, 0, 0);
            home.add(playButton, 0, 3);
            home.add(rulesButton, 0, 4);
            home.add(quitButton, 0, 5);
            home.add(nbsp, 0, 6);

            // Classe CSS
            t.getStyleClass().add("title");
            t.getStyleClass().add("mrg-left-30");
            home.getStyleClass().add("grid");
            nbsp.getStyleClass().add("nbsp-50");
        }
        getChildren().add(home);
    }

    @Override
    protected void createController() {
        playButton.setOnAction(e -> fireEvent(new ChangePaneEvent(ChangePaneEvent.HOME_TO_PLAY)));
        rulesButton.setOnAction(e -> fireEvent(new ChangePaneEvent(ChangePaneEvent.HOME_TO_RULES)));
        quitButton.setOnAction(e -> System.exit(0));
    }
}