package view.panes;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import model.Game;
import model.GameModel;
import view.util.TransitionUtils;

public class EchapGamePane extends AbstractOthelloPane {
    private Button exitButton;
    private Button resumeButton;
    private Label messageLabel;
    private GamePane gamePane;

    public EchapGamePane(Game model, GamePane gamePane) {
        super(model);
        this.gamePane = gamePane;
        setVisible(false);
    }

    @Override
    protected void createView() {
        exitButton = new Button("Sortir");
        resumeButton = new Button("Reprendre");
        messageLabel = new Label("Voulez-vous quitter la partie ?");
    }

    @Override
    protected void placeComponents() {
        BorderPane p = new BorderPane();
        {
            p.setCenter(messageLabel);

            HBox v = new HBox();
            {
                v.getChildren().addAll(exitButton, resumeButton);

                // Class CSS
                v.getStyleClass().add("btn-end");
                exitButton.getStyleClass().add("btn-red");
                resumeButton.getStyleClass().add("btn-green");
            }
            p.setBottom(v);

            // Class CSS
            p.getStyleClass().add("pane-end");
            messageLabel.getStyleClass().add("lbl-end");
        }
        getChildren().add(p);
    }

    @Override
    protected void createController() {
        GameModel game = model.getCurrentGame();

        exitButton.setOnAction(e -> {
            gamePane.disablePlayButton(false);
            gamePane.disableNextButton(false);
            gamePane.disableQuitButton(false);

            game.pause();
            setVisible(false);
            model.getLauncher().switchSceneHome();
        });

        resumeButton.setOnAction(e -> {
            gamePane.disableQuitButton(false);
            gamePane.disablePlayButton(false);
            if (game.isPause()) {
                gamePane.disableNextButton(false);
            } else {
                gamePane.disableNextButton(true);
            }
            if (!game.getPlayer1().isAI() || !game.getPlayer2().isAI()) {
                gamePane.disablePlayButton(true);
                gamePane.disableNextButton(true);
            }
            TransitionUtils.fadeOut(this);
        });
    }
}
