package view.panes;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import model.Game;
import model.GameModel;
import model.player.Player;
import view.Logger;
import view.util.TransitionUtils;

public class EndGamePane extends AbstractOthelloPane {
    private Button exitButton;
    private Button restartButton;
    private VBox messageBox;

    EndGamePane(Game model) {
        super(model);
        setVisible(false);
    }

    void triggerWinner(Player winner){
        Platform.runLater(() -> {
            messageBox.getChildren().clear();
            if (winner != null) {

                Label playerName = new Label(winner + " a remporté la partie !");
                Label playerScore = new Label(model.getCurrentGame().getPlayerScore(winner) + "");

                Circle playerPawn;
                playerPawn = new Circle(25, winner.getColor().getColorValue());
                playerPawn.setStrokeWidth(3);
                playerPawn.setStroke(Color.GRAY);

                playerScore.setTextFill(model.getCurrentGame().getOpponentOf(winner).getColor().getColorValue());

                // Class CSS
                playerName.getStyleClass().add("lbl-end");
                playerScore.getStyleClass().add("lbl-score");
                messageBox.getStyleClass().add("player-box-game");

                StackPane stack = new StackPane();
                {
                    stack.getChildren().addAll(playerPawn, playerScore);
                }
                messageBox.getChildren().addAll(stack, playerName);
            } else {
                Label l = new Label("La partie est nulle !");
                messageBox.getChildren().add(l);

                // Class CSS
                l.getStyleClass().add("lbl-end");
            }
            messageBox.setAlignment(Pos.CENTER);
        });

    }

    @Override
    protected void createView() {
        exitButton = new Button("Sortir");
        restartButton = new Button("Recommencer");
        messageBox = new VBox();
    }

    @Override
    protected void placeComponents() {
        BorderPane p = new BorderPane();
        {
            p.setCenter(messageBox);

            HBox v = new HBox();
            {
                v.getChildren().addAll(exitButton, restartButton);

                // Class CSS
                v.getStyleClass().add("btn-end");
                exitButton.getStyleClass().add("btn-red");
                restartButton.getStyleClass().add("btn-green");
            }
            p.setBottom(v);

            // Class CSS
            p.getStyleClass().add("pane-end");
        }
        getChildren().add(p);
    }

    @Override
    protected void createController() {
        GameModel game = model.getCurrentGame();

        exitButton.setOnAction(e -> {
            game.pause();
            setVisible(false);
            model.getLauncher().switchSceneHome();
        });

        restartButton.setOnAction(e -> {
            Logger.getInstance().refresh();
            game.restart();
            TransitionUtils.fadeOut(this);
        });
    }
}
