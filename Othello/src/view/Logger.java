package view;

import javafx.application.Platform;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class Logger {
    private static Logger ourInstance = new Logger();
    private TextFlow textFlow = new TextFlow();
    private int lastRound;

    private Logger() {
    }

    public static Logger getInstance() {
        return ourInstance;
    }

    public void addLog(String s) {
        Platform.runLater((() -> {
            Text t = new Text(s + "\n");

            // Class CSS
            t.getStyleClass().add("txt-flow-log");

            textFlow.getChildren().add(t);
        }));
    }

    public void addSeparator(int round) {
        if (lastRound == round) {
            return;
        }
        lastRound = round ;
        Platform.runLater(() -> {
            Text t = new Text(String.format("\n\n\n-------------------- Tour n°%d --------------------\n\n", round));
            t.setStyle("-fx-font-weight: bold");

            // Class CSS
            t.getStyleClass().add("txt-flow-log");

            textFlow.getChildren().add(t);
        });
    }

    public void refresh() {
        textFlow.getChildren().clear();
        lastRound = 0;
    }

    public TextFlow getTextFlow() {
        return textFlow;
    }
}
