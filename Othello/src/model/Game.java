package model;

import model.listener.GameListener;
import model.listener.GameSupport;
import model.player.pawn.PawnColor;
import model.player.Player;
import model.player.PlayerType;
import model.player.ai.Level;
import view.Othello;
import view.panes.AbstractOthelloPane;

public class Game {
    private GameModel game;
    private GameSupport support;
    private Othello launcher;

    public Game(Othello launcher) {
        this.support = new GameSupport();
        this.launcher = launcher;
    }

    public GameModel getCurrentGame() {
        return game;
    }

    public Player generatePlayer(PlayerType type, Level level, PawnColor color) {
        return type.getConcretePlayer(level, color);
    }

    public void startNewGame(Player p1, Player p2) {
        game = new GameModel(new Board(p1, p2), p1, p2);
        support.fireContentChanged();
        game.start();
    }

    public void addGameListener(GameListener listener) {
        support.addGameListener(listener);
    }

    public void removeGameListener(GameListener listener) {
        support.removeGameListener(listener);
    }

    public Othello getLauncher() {
        return launcher;
    }
}