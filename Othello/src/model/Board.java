package model;

import model.player.pawn.Pawn;
import model.player.pawn.PawnColor;
import model.player.Player;
import model.util.Coordinate;

import java.util.LinkedList;
import java.util.List;

public class Board {


    public final static int WIDTH = 8;
    public final static int HEIGHT = 8;

    private Pawn[][] board;

    Board(Player player1, Player player2) {
        board = new Pawn[WIDTH][HEIGHT];
        board[3][3] = new Pawn(player2, new Coordinate(3,3));
        board[3][4] = new Pawn(player1, new Coordinate(3,4));
        board[4][3] = new Pawn(player1, new Coordinate(4,3));
        board[4][4] = new Pawn(player2, new Coordinate(4,4));
    }

    Board(Pawn[][] board) {
        this.board = board;
    }

    public Pawn[][] getBoard() {
        return board;
    }

    Pawn getPawnAt(Coordinate coord) {
        return board[coord.getX()][coord.getY()];
    }

    public Player getPlayerAt(Coordinate coord) {
        if (board[coord.getX()][coord.getY()] == null) {
            return null;
        }
        return board[coord.getX()][coord.getY()].getPlayer();
    }

    public PawnColor getColorOf(Player player) {
        return player.getColor();
    }

    void placePawn(Coordinate coord, Pawn p){
        this.board[coord.getX()][coord.getY()] = p;
    }

    public List<Coordinate> getCoordinatePlayer(Player p) {
        List<Coordinate> list = new LinkedList<>();
        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                Coordinate c = new Coordinate(i, j);
                if (getPlayerAt(c) == p) {
                    list.add(c);
                }
            }
        }
        return list;
    }

    public Board clone() {
        Pawn[][] b = new Pawn[WIDTH][HEIGHT];
        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                b[i][j] = getPawnAt(new Coordinate(i, j));
            }
        }
        return new Board(b);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != this.getClass()) {
            return false;
        }
        Board board = (Board)obj;
        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                if (board.getPlayerAt(new Coordinate(i, j)) != this.getPlayerAt(new Coordinate(i, j))) {
                    return false;
                }
            }
        }
        return true;
    }

}