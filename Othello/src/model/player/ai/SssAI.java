package model.player.ai;

import model.GameModel;
import model.exception.InvalidMoveException;
import model.player.Player;
import model.player.pawn.Pawn;
import model.player.pawn.PawnColor;
import model.util.Coordinate;

import java.util.LinkedList;
import java.util.List;



public class SssAI extends AbstractAI implements Player {

    public SssAI(Level level, PawnColor color) {
        super(level, color);
    }

    public void play(GameModel model) {
        if (model == null) {
            return;
        }


        // On regarde les mouvements possible
        List<Coordinate> l = model.getPossibleMove(this);
        if (l.size() == 0) {
            return;
        }



        // On crée un thread pour calculer chaque branche
        SssThread[] thread = new SssThread[l.size()];
        int i = 0;
        for(Coordinate p : l) {
            List<Quintuple> list = new LinkedList<>();
            GameModel b =  model.clone();
            try {
                b.playAt(p, new Pawn(b.getCurrentPlayer(), p));
            } catch (InvalidMoveException e) {
                // N'arrivera pas
            }


            List<GameModel> parents = new LinkedList<>();
            parents.add(model.clone());
            list.add(new Quintuple(b.clone(), true, Integer.MAX_VALUE, parents, 2));


            SssThread t = new SssThread(list, b, this);
            t.start();
            thread[i] = t;
            i++;
        }

        // on stop les thread proprement au bout de 20 secondes, s'il ne se sont par stoppé seul
        Thread f = new Thread(() -> {
            for(SssThread t : thread) {
                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        f.start();
        try {
            f.join(15000);
            for(SssThread t : thread) {
                t.terminate();
            }
        } catch (InterruptedException e) {
            // Nothing
        }


        // On sélectionne la branche ayant la plus grande valeur
        int maxAlphaBeta = Integer.MIN_VALUE;
        Coordinate currentCoordinate = null;
        i = 0;
        int numberEquivalent = 1;
        for (SssThread t : thread) {
            addLog(l.get(i) + "   \tvaleur = " + t.getResult());
            if (t.getResult() >= maxAlphaBeta) {
                if (t.getResult() == maxAlphaBeta) {
                    if (currentCoordinate == null || Math.random() <= (1/numberEquivalent)) {
                        currentCoordinate = l.get(i);
                        numberEquivalent++;
                    }
                } else {
                    maxAlphaBeta = t.getResult();
                    currentCoordinate = l.get(i);
                }
            }

            i++;
        }

        addLog("\n" + this + " a joué en " + currentCoordinate);
        try {
            playAt(currentCoordinate);
        } catch (InvalidMoveException e) {
            // N'arrivera pas pour les IA
        }
    }


    // TOOLS

    private class SssThread extends Thread {

        private List<Quintuple> list;
        private GameModel model;
        private AbstractAI player;
        private int result;
        private Boolean terminate;


        private SssThread(List<Quintuple> list, GameModel model, AbstractAI player){
            super(null, null, "TT", 2000000);
            this.list = list;
            this.model = model;
            this.player = player;
            this.result = 0;
            this.terminate = false;

        }
        public void terminate() {
            terminate = true;
        }

        public void run(){
            result = sss(list, model);
        }

        private int getResult() {
            return result;
        }

        private int sss(List<Quintuple> l, GameModel b) {
            while (!l.get(0).getNode().equals(b) || l.get(0).isAlive()) {
                if (terminate) {
                    return getHeuristic(b);
                }
                Quintuple t = l.get(0);
                int depth = t.getDepth();
                l.remove(0);
                if (t.isAlive()) {
                    if (t.getNode().getPossibleMove(t.getNode().getCurrentPlayer()).size() == 0) {
                        result = Math.min(result, b.getPlayerScore(player) - b.getPlayerScore(b.getOpponentOf(player)));
                        //if (true) {
                        if (!(t.getNode().getCurrentPlayer() != player)) {
                            insert(new Quintuple(t.getNode(), false, Math.min(t.getValue(), (b.getPlayerScore(player) - b.getPlayerScore(b.getOpponentOf(player))) * (-1)), t.getParents(), depth), l);
                        } else {
                            insert(new Quintuple(t.getNode(), false, Math.min(t.getValue(), b.getPlayerScore(player) - b.getPlayerScore(b.getOpponentOf(player))), t.getParents(), depth), l);
                        }
                    } else if(depth <= 0) {
                        result = Math.min(result, getHeuristic(t.getNode()));
                        //if (false) {
                        if (!(t.getNode().getCurrentPlayer() != player)) {
                            insert(new Quintuple(t.getNode(), false, Math.min(t.getValue(), getHeuristic(t.getNode()) * (-1)), t.getParents(), depth), l);
                        } else {
                            insert(new Quintuple(t.getNode(), false, Math.min(t.getValue(), getHeuristic(t.getNode())), t.getParents(), depth), l);
                        }

                    } else {
                        for(Coordinate ignored : t.getNode().getPossibleMove(t.getNode().getCurrentPlayer())) {
                            GameModel f = t.getNode().clone();
                            try {
                                f.getCurrentPlayer().playAt(f.getPossibleMove(f.getCurrentPlayer()).get(0));
                            } catch (InvalidMoveException e) {
                                // N'arrivera pas
                            }
                            List<GameModel> parents = new LinkedList<>(t.getParents());

                            parents.add(t.getNode());
                            insert(new Quintuple(f, true, t.getValue(), parents, depth - 1), l);
                        }
                    }
                } else {

                    if (t.getNode().getCurrentPlayer() == player) {
                        if (t.getFirstBrother() != null) {
                            insert(new Quintuple(t.getFirstBrother(), true, t.getValue(), t.getParents(), depth), l);
                        } else {
                            List<GameModel> parents = new LinkedList<>(t.getParents());
                            GameModel f = parents.get(parents.size() - 1);
                            parents.remove(parents.size() - 1);
                            insert(new Quintuple(f, false, t.getValue(), parents, depth + 1), l);
                        }
                    } else {
                        List<GameModel> parents = new LinkedList<>(t.getParents());
                        GameModel f = parents.get(parents.size() - 1);
                        parents.remove(parents.size() - 1);
                        insert(new Quintuple(f, false, t.getValue(), parents, depth + 1), l);

                        for (Quintuple q : new LinkedList<>(l)) {
                            for (GameModel a :q.getParents()) {
                                if(a.equals(t.getNode())) {
                                    l.remove(q);
                                }

                            }
                        }
                    }
                }
            }
            return l.get(0).getValue();
        }

        private void insert(Quintuple t, List<Quintuple> l) {
            if (t.isAlive()) {
                int i = 0;
                while(i < l.size() && l.get(i).isAlive()) {
                    i++;
                }
                l.add(i, t);
            } else {
                if (l.size() == 0) {
                    l.add(t);
                } else {
                    int i = 0;
                    while (i < l.size() && (l.get(i).isAlive() || l.get(i).getValue() > t.getValue())) {
                        i++;
                    }
                    l.add(i, t);
                }
            }
        }
    }

    private class Quintuple {

        private GameModel model;
        private List<GameModel> parents;
        private Boolean alive;
        private int value;
        private int depth;

        private Quintuple(GameModel nomodelde, Boolean alive, int value, List<GameModel> parents, int depth) {
            this.model = nomodelde;
            this.alive = alive;
            this.value = value;
            this.parents = parents;
            this.depth = depth;
        }

        private int getDepth() {
            return depth;
        }
        private GameModel getNode() {
            return model;
        }

        private Boolean isAlive() {
            return alive;
        }

        private int getValue() {
            return value;
        }

        private List<GameModel> getParents() {
            return parents;
        }

        private GameModel getFirstBrother() {
            GameModel parent = parents.get(parents.size() -  1);
            Boolean returnNext = false;
            for (Coordinate c : parent.getPossibleMove(parent.getCurrentPlayer())) {
                try {
                    parent.playAt(c, new Pawn(parent.getCurrentPlayer(), c));
                } catch (InvalidMoveException e) {
                    // N'arrivera pas
                }
                if (returnNext) {
                    return parent;
                }
                if (parent.equals(model)) {
                    returnNext = true;
                }
            }
            return null;
        }
    }

    public String toString() {
        return String.format("SSS* (%s)", getLevel());
    }
}
