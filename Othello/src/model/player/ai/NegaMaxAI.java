package model.player.ai;

import model.GameModel;
import model.exception.InvalidMoveException;
import model.player.Player;
import model.player.pawn.Pawn;
import model.player.pawn.PawnColor;
import model.util.Coordinate;

import java.util.List;

public class NegaMaxAI extends AbstractAI implements Player {

    public NegaMaxAI(Level level, PawnColor color) {
        super(level, color);
    }

    public void play(GameModel model) {
        if (model == null) {
            return;
        }

        // On regarde les mouvements possible
        List<Coordinate> l = model.getPossibleMove(this);
        if (l.size() == 0) {
            return;
        }


        // On crée un thread pour calculer chaque branche
        NegaMaxThread[] thread = new NegaMaxThread[l.size()];
        int i = 0;
        for(Coordinate p : l) {
            NegaMaxThread t = new NegaMaxThread(p, model.clone(), this,  3);
            t.start();
            thread[i] = t;
            i++;
        }



        // on stop les thread proprement au bout de 20 secondes, s'il ne se sont par stoppé seul
        Thread f = new Thread(() -> {
            for(NegaMaxThread t : thread) {
                try {
                    t.join();
                } catch (InterruptedException e) {
                    // Nothing
                }
            }
        });
        f.start();
        try {
            f.join(15000);
            for(NegaMaxThread t : thread) {
                t.terminate();
            }
        } catch (InterruptedException e) {
            // Nothing
        }

        // On sélectionne la branche ayant la plus grande valeur
        i = 0;
        Coordinate currentCoordinate = null;
        int maxVal = Integer.MIN_VALUE;
        int numberEquivalent = 1;
        for (NegaMaxThread t : thread) {
            addLog(l.get(i) + "   \tvaleur = " + t.getResult());
            if (t.getResult() >= maxVal) {
                if (t.getResult() == maxVal) {
                    if (currentCoordinate == null || Math.random() <= (1/numberEquivalent)) {
                        numberEquivalent++;
                        currentCoordinate = l.get(i);
                    }
                } else {
                    currentCoordinate = l.get(i);
                    maxVal = t.getResult();
                }
            }

            i++;
        }
        addLog("\n" + this + " a joué en " + currentCoordinate);

        // On joue le coup
        try {
            playAt(currentCoordinate);
        } catch (InvalidMoveException e) {
            // N'arrivera pas pour les IA
        }
    }



    // TOOLS

    private class NegaMaxThread extends Thread {

        private Coordinate p;
        private GameModel model;
        private int depth;
        private AbstractAI player;
        private int result;
        private Boolean terminate;

        private NegaMaxThread(Coordinate p, GameModel model, AbstractAI player, int depth){
            super(null, null, "TT", 2000000);
            this.p = p;
            this.model = model;
            this.player = player;
            this.depth = depth;
            this.terminate = false;

            result = 0;
        }

        public void terminate() {
            terminate = true;
        }

        public void run(){
            result = negaMax(p, model.clone(), depth);
        }

        private int getResult() {
            return result;
        }

        // Exécution de negaMax
        private int negaMax(Coordinate c, GameModel b, int depth) {
            if (terminate) {
                return getHeuristic(b);
            }
            GameModel gm = b.clone();
            try {
                try {
                    gm.playAt(c, new Pawn(gm.getCurrentPlayer(), c));
                } catch (InvalidMoveException e) {
                    return Integer.MIN_VALUE;
                }
                if (gm.getPossibleMove(gm.getCurrentPlayer()).size() == 0) {
                    return gm.getPlayerScore(player) - gm.getPlayerScore(gm.getOpponentOf(player));
                }
                if (depth <= 0) {
                    return getHeuristic(gm);
                }
                int val = Integer.MIN_VALUE;
                for (Coordinate coordinate : gm.getPossibleMove(gm.getCurrentPlayer())) {
                    val = Math.max(val, -negaMax(coordinate, gm.clone(), depth--));
                }
                result = Math.min(result, val);
                return val;
            } catch (StackOverflowError e ) {
                try {
                    return getHeuristic(gm);
                } catch (StackOverflowError e1 ) {
                    return Integer.MIN_VALUE;
                }
            }
        }
    }

    public String toString() {
        return String.format("NegaMax (%s)", getLevel());
    }
}
