package model.player.ai;

import model.Board;
import model.GameModel;
import model.listener.GameModelListener;
import model.player.Human;
import model.player.pawn.PawnColor;
import model.player.Player;
import model.util.Coordinate;
import view.Logger;

import java.util.Random;

public abstract class AbstractAI extends Human implements Player {
    private int[][] heuristic;
    private GameModelListener listener;
    private Level level;
    private Boolean stop;

    AbstractAI(Level level, PawnColor color)  {
        super(color);
        heuristic = new int[Board.WIDTH][Board.HEIGHT];

        int[] column1 = {500, -150, 30, 10, 10, 30, -150, 500};
        int[] column2 = {-150, -250, 0, 0, 0, 0, -250, -150};
        int[] column3 = {30, 0, 1, 2, 2, 1, 0, 30};
        int[] column4 = {10, 0, 2, 16, 16, 2, 0, 10};

        heuristic[0] = column1;
        heuristic[1] = column2;
        heuristic[2] = column3;
        heuristic[3] = column4;

        heuristic[4] = column4;
        heuristic[5] = column3;
        heuristic[6] = column2;
        heuristic[7] = column1;

        this.level = level;

        stop = true;

        Player p = this;
        listener = new GameModelListener() {
            @Override
            public void gameEnded() {
                pause();
            }

            @Override
            public void gameChanged() {
                if (stop) {
                    return;
                }
                if (getGameModel().getCurrentPlayer() == p) {
                    new Thread(() -> {
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            // Nothing
                        }
                        play(getGameModel().clone());
                    }).start();
                }
            }

            @Override
            public void gameStarted() {
                // Nothing
            }
        };
    }

    public void setBoard(GameModel b) {
        super.setModel(b);
    }

    protected void play(GameModel b) {
        throw new UnsupportedOperationException("");
    }

    int getHeuristic(GameModel b) {
        int sum = 0;
        switch (level) {
            case EASY:
                Random rand = new Random();
                sum += rand.nextInt((15 - 1) + 1) + 15 + b.getPlayerScore(b.getCurrentPlayer());
                sum -= b.getPlayerScore(b.getOpponentOf(b.getCurrentPlayer()));
                break;

            case MEDIUM:
                sum += b.getPlayerScore(b.getCurrentPlayer());
                sum -= b.getPlayerScore(b.getOpponentOf(b.getCurrentPlayer()));
                break;

            case HARD:
                for (Coordinate coord : b.getBoard().getCoordinatePlayer(b.getCurrentPlayer())) {
                    sum -= heuristic[coord.getX()][coord.getY()];
                }
                for (Coordinate coord : b.getBoard().getCoordinatePlayer(b.getOpponentOf(b.getCurrentPlayer()))) {
                    sum += heuristic[coord.getX()][coord.getY()];
                }
                break;
        }
        return sum;
    }


    protected void addLog(String log) {
        Logger.getInstance().addLog(log);
    }


    public void start() {
        getGameModel().addBoardListener(listener);
        stop = false;
        next();
    }

    public void pause() {
        getGameModel().removeBoardListener(listener);
        stop = true;
    }

    public void next() {
        new Thread(() -> play(getGameModel())).start();
    }

    public Level getLevel() {
        return level;
    }

    @Override
    public boolean isAI() {
        return true;
    }
}
