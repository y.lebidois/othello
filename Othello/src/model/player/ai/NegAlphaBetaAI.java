package model.player.ai;

import model.GameModel;
import model.exception.InvalidMoveException;
import model.player.Player;
import model.player.pawn.Pawn;
import model.player.pawn.PawnColor;
import model.util.Coordinate;

import java.util.List;

public class NegAlphaBetaAI extends AbstractAI implements Player {

    public NegAlphaBetaAI(Level level, PawnColor color) {
        super(level, color);
    }

    public void play(GameModel model) {
        if (model == null) {
            return;
        }

        // On regarde les mouvements possible
        List<Coordinate> l = model.getPossibleMove(this);
        if (l.size() == 0) {
            return;
        }

        // On crée un thread pour calculer chaque branche
        NegAlphaBetaThread[] thread = new NegAlphaBetaThread[l.size()];
        int i = 0;
        for(Coordinate p : l) {
            NegAlphaBetaThread t = new NegAlphaBetaThread(p, Integer.MIN_VALUE, Integer.MAX_VALUE, model.clone(), this,  3);
            t.start();
            thread[i] = t;
            i++;
        }


        // on stop les thread proprement au bout de 20 secondes, s'il ne se sont par stoppé seul
        Thread f = new Thread(() -> {
            for(NegAlphaBetaThread t : thread) {
                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        f.start();
        try {
            f.join(16000);
            for(NegAlphaBetaThread t : thread) {
                t.terminate();
            }
        } catch (InterruptedException e) {
            // Nothing
        }


        // On sélectionne la branche ayant la plus grande valeur
        i = 0;
        int maxAlphaBeta = Integer.MIN_VALUE;
        Coordinate currentCoordinate = null;
        int numberEquivalent = 1;
        for (NegAlphaBetaThread t : thread) {
            addLog(l.get(i) + "   \tvaleur = " + t.getResult());
            if (t.getResult() >= maxAlphaBeta) {
                if (t.getResult() == maxAlphaBeta) {
                    if (currentCoordinate == null || Math.random() <= (1/numberEquivalent)) {
                        currentCoordinate = l.get(i);
                        numberEquivalent++;
                    }
                } else {
                    currentCoordinate = l.get(i);
                    maxAlphaBeta = t.getResult();
                }
            }
            i++;
        }

        addLog("\n" + this + " a joué en " + currentCoordinate);
        // On joue le coup
        try {
            playAt(currentCoordinate);
        } catch (InvalidMoveException e) {
            // N'arrivera pas pour les IA
        }
    }

    // TOOLS

    private class NegAlphaBetaThread extends Thread {

        private Coordinate p;
        private int alpha;
        private int beta;
        private GameModel model;
        private int depth;
        private AbstractAI player;
        private int result;
        private Boolean terminate;

        private NegAlphaBetaThread(Coordinate p, int alpha, int beta, GameModel model, AbstractAI player, int depth){
            super(null, null, "TT", 2000000);
            this.p = p;
            this.alpha = alpha;
            this.beta = beta;
            this.model = model;
            this.player = player;
            this.depth = depth;
            this.terminate = false;
            result = 0;
        }

        public void terminate() {
            terminate = true;
        }
        public void run(){
            result = negAlphaBeta(p, alpha, beta, model, depth);
        }

        private int getResult() {
            return result;
        }

        // Exécution de negAlphaBeta
        private int negAlphaBeta(Coordinate c, int alpha, int beta, GameModel b, int depth) {
            if (terminate) {
                return getHeuristic(b);
            }
            GameModel gm = b.clone();
            try {
                try {
                    gm.playAt(c, new Pawn(gm.getCurrentPlayer(), c));
                } catch (InvalidMoveException e) {
                    return Integer.MIN_VALUE;
                }
                if (gm.getPossibleMove(gm.getCurrentPlayer()).size() == 0) {
                    return gm.getPlayerScore(player) - gm.getPlayerScore(gm.getOpponentOf(player));
                }
                if (depth <= 0) {
                    return getHeuristic(gm);
                }
                int best = Integer.MIN_VALUE;
                for (Coordinate coordinate : gm.getPossibleMove(gm.getCurrentPlayer())) {
                    int v = -negAlphaBeta(coordinate, -beta, -alpha, gm.clone(), depth--);
                    if (v > best) {
                        best = v;
                        if (best > alpha) {
                            alpha = best;
                            if (alpha >= beta) {
                                return best;
                            }
                        }
                    }

                }
                result = Math.min(result, best);
                return best;
            } catch (StackOverflowError e ) {
                try {
                     return getHeuristic(gm);
                } catch (StackOverflowError e1 ) {
                    return Integer.MIN_VALUE;
                }
            }
        }
    }

    public String toString() {
        return String.format("NegAlphaBeta (%s)", getLevel());
    }


}
