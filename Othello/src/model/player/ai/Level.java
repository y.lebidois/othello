package model.player.ai;

public enum Level {
    EASY("Facile"),
    MEDIUM("Moyen"),
    HARD("Difficile");

    private String name;

    Level(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }
}
