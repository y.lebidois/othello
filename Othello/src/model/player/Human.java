package model.player;

import model.GameModel;
import model.exception.InvalidMoveException;
import model.player.pawn.Pawn;
import model.player.pawn.PawnColor;
import model.util.Coordinate;

public class Human implements  Player {

    protected GameModel model;
    private PawnColor color;

    public Human(PawnColor color) {
        this.color = color;
    }

    @Override
    public void setModel(GameModel model) {
        this.model = model;
    }

    public GameModel getGameModel() {
        return model;
    }

    @Override
    public void playAt(Coordinate c) throws InvalidMoveException {
        model.playAt(c, new Pawn(this, c));
    }

    @Override
    public PawnColor getColor() {
        return color;
    }

    @Override
    public void start() {
        // rien
    }

    @Override
    public void pause() {
        // rien
    }

    @Override
    public void next() {
        // rien
    }

    @Override
    public boolean isAI() {
        return false;
    }

    public String toString() {
        return "Humain";
    }
}
