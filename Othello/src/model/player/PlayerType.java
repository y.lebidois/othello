package model.player;

import model.player.ai.Level;
import model.player.ai.NegAlphaBetaAI;
import model.player.ai.NegaMaxAI;
import model.player.ai.SssAI;
import model.player.pawn.PawnColor;

public enum PlayerType {
    HUMAN("Humain"),
    NEG_ALPHABETA("NegAlphaBeta"),
    NEGAMAX("NégaMax"),
    SSS("SSS*");

    private String name;

    PlayerType(String name) {
        this.name = name;
    }

    public Player getConcretePlayer(Level level, PawnColor color) {
        switch (this) {
            case HUMAN:
                return new Human(color);
            case NEG_ALPHABETA:
                return new NegAlphaBetaAI(level, color);
            case NEGAMAX:
                return new NegaMaxAI(level, color);
            case SSS:
                return new SssAI(level, color);

            default:
                throw new IllegalArgumentException("IA Inconnue");
        }
    }

    public String toString() {
        return name;
    }
}