package model.player.pawn;

import model.player.Player;
import model.util.Coordinate;

public class Pawn {

    private Player player;
    private Coordinate coord;


    public Pawn(Player player, Coordinate coord) {
        this.player = player;
        this.coord = coord;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Coordinate getCoord() {
        return coord;
    }

    public void setCoord(Coordinate coord) {
        this.coord = coord;
    }
}
