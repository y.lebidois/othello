package model.player.pawn;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public enum PawnColor {
    BLACK,
    WHITE;

    public static PawnColor getPawnColorOf(Paint color) {
        if (color == Color.BLACK) return BLACK;
        else if (color == Color.WHITE) return WHITE;
        else throw new IllegalArgumentException("Mauvaise couleur.");
    }

    public Color getColorValue() {
        switch (this) {
            case BLACK:
                return Color.BLACK;
            case WHITE:
                return Color.WHITE;
            default:
                throw new IllegalArgumentException("Mauvaise couleur.");
        }
    }

    @Override
    public String toString() {
        switch (this) {
            case BLACK:
                return "Noir";
            case WHITE:
                return "Blanc";
            default:
                throw new IllegalArgumentException("Mauvaise couleur.");
        }
    }
}
