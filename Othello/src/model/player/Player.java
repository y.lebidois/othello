package model.player;

import model.GameModel;
import model.exception.InvalidMoveException;
import model.player.pawn.PawnColor;
import model.util.Coordinate;

public interface Player {

    void setModel(GameModel b);
    void playAt(Coordinate c) throws InvalidMoveException;
    PawnColor getColor();

    void start();
    void pause();
    void next();

    boolean isAI();

    String toString();
}
