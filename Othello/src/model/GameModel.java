package model;

import model.exception.InvalidMoveException;
import model.listener.GameModelListener;
import model.listener.GameModelSupport;
import model.player.Player;
import model.player.pawn.Pawn;
import model.player.pawn.PawnColor;
import model.util.Coordinate;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class GameModel {
    private Board board;
    private Player player1;
    private Player player2;

    private Player currentPlayer;
    private int round;
    private boolean pause;

    private GameModelSupport gameModelSupport;

    public GameModel(Board board, Player player1, Player player2) {
        this.board = board;
        this.player1 = player1;
        this.player2 = player2;

        if (player1.getColor() == PawnColor.BLACK) {
            this.currentPlayer = player1;
        } else {
            this.currentPlayer = player2;
        }
        this.round = 1;
        this.gameModelSupport = new GameModelSupport();

        player1.setModel(this);
        player2.setModel(this);
    }

    private GameModel(Board board, Player player1, Player player2, Player currentPlayer) {
        this.board = board;
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = currentPlayer;

        gameModelSupport = new GameModelSupport();
    }

    public void restart() {
        board = new Board(player1, player2);
        round = 1;
        if (player1.getColor() == PawnColor.BLACK) {
            this.currentPlayer = player1;
        } else {
            this.currentPlayer = player2;
        }

        gameModelSupport.fireGameChanged();
        start();
    }

    public void start() {
        pause = false;

        player1.start();
        player2.start();
        gameModelSupport.fireGameStarted();
    }

    public void pause() {
        pause = true;

        player1.pause();
        player2.pause();
    }

    public void next() {
        player1.next();
        player2.next();
    }

    public Player getOpponentOf(Player p) {
        return p == player1 ? player2 : player1;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public int getRound() {
        return round;
    }

    public void addBoardListener(GameModelListener listener) {
        gameModelSupport.addBoardListener(listener);
    }

    public void removeBoardListener(GameModelListener listener) {
        gameModelSupport.removeBoardListener(listener);
    }

    public int getPlayerScore(Player p) {
        int score = 0;
        for (int i = 0; i < Board.WIDTH; i++) {
            for (int j = 0; j < Board.HEIGHT; j++) {
                if (board.getPlayerAt(new Coordinate(i, j)) == p) {
                    score++;
                }
            }
        }
        return score;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != this.getClass()) {
            return false;
        }
        GameModel model = (GameModel) obj;
        if (model.getCurrentPlayer() != this.getCurrentPlayer()
                || model.getPlayer1() != this.getPlayer1()
                || model.getPlayer2() != this.getPlayer2()) {
            return false;
        }
        for (int i = 0; i < Board.WIDTH; i++) {
            for (int j = 0; j < Board.HEIGHT; j++) {
                if (model.getBoard().getPlayerAt(new Coordinate(i, j)) != this.getBoard().getPlayerAt(new Coordinate(i, j))) {
                    return false;
                }
            }
        }
        return true;
    }


    public GameModel clone() {
        Pawn[][] b = new Pawn[Board.WIDTH][Board.HEIGHT];
        for (int i = 0; i < Board.WIDTH; i++) {
            for (int j = 0; j < Board.HEIGHT; j++) {
                b[i][j] = this.getBoard().getPawnAt(new Coordinate(i, j));
            }
        }
        return new GameModel(new Board(b), player1, player2, currentPlayer);
    }

    public void playAt(Coordinate coord, Pawn p) throws InvalidMoveException {
        for (Coordinate c : getCoordinatePlayAt(coord, p.getPlayer())) {
            board.placePawn(c, p);
        }

        currentPlayer = getOpponentOf(currentPlayer);
        if (getPossibleMove(currentPlayer).size() == 0) {
            currentPlayer = getOpponentOf(currentPlayer);
            if (getPossibleMove(currentPlayer).size() == 0) {
                currentPlayer = null;
                gameModelSupport.fireGameEnded();

            }
        }
        round++;
        this.gameModelSupport.fireGameChanged();
    }

    private Boolean canPlayAt(Coordinate coord, Player p) {
        if (!isValidCoordinate(coord) || currentPlayer != p || board.getPlayerAt(coord) != null) {
            return false;
        }
        return (canPlayAtWest(coord, p) || canPlayAtEast(coord, p) || canPlayAtNorth(coord, p) || canPlayAtSouth(coord, p) || canPlayAtNorthWest(coord, p) || canPlayAtNorthEast(coord, p) || canPlayAtSouthWest(coord, p) || canPlayAtSouthEast(coord, p));
    }

    public List<Coordinate> getPossibleMove(Player p) {
        List<Coordinate> l = new LinkedList<>();
        for (int i = 0; i < Board.WIDTH; i++) {
            for (int j = 0; j < Board.HEIGHT; j++) {
                Coordinate coord = new Coordinate(i, j);
                if (canPlayAt(coord, p)) {
                    l.add(coord);
                }
            }
        }
        return l;
    }

    private Boolean isValidCoordinate(Coordinate coord) {
        return !(coord.getX() < 0 || coord.getX() >= Board.WIDTH || coord.getY() < 0 || coord.getY() >= Board.HEIGHT);
    }

    private Set<Coordinate> getCoordinatePlayAt(Coordinate coord, Player p) throws InvalidMoveException {
        Set<Coordinate> coords = new HashSet<>();
        if (canPlayAt(coord, p)) {
            coords.add(coord);

            coords.addAll(getCoordinatePlayAtWest(coord, p));
            coords.addAll(getCoordinatePlayAtEast(coord, p));
            coords.addAll(getCoordinatePlayAtNorth(coord, p));
            coords.addAll(getCoordinatePlayAtSouth(coord, p));
            coords.addAll(getCoordinatePlayAtNorthWest(coord, p));
            coords.addAll(getCoordinatePlayAtNorthEast(coord, p));
            coords.addAll(getCoordinatePlayAtSouthWest(coord, p));
            coords.addAll(getCoordinatePlayAtSouthEast(coord, p));

            return coords;
        } else {
            throw new InvalidMoveException();
        }
    }

    private Set<Coordinate> getCoordinatePlayAtWest(Coordinate coord, Player p) {
        int x = coord.getX();
        int y = coord.getY();
        Set<Coordinate> coords = new HashSet<>();
        if (canPlayAtWest(coord, p)) {
            x--;

            Coordinate c = new Coordinate(x, y);
            while (isValidCoordinate(c) && board.getPlayerAt(new Coordinate(x, y)) == getOpponentOf(p)) {
                coords.add(c);
                x--;
                c = new Coordinate(x, y);
            }
        }
        return coords;
    }

    private Set<Coordinate> getCoordinatePlayAtNorthWest(Coordinate coord, Player p) {
        int x = coord.getX();
        int y = coord.getY();
        Set<Coordinate> coords = new HashSet<>();
        if (canPlayAtNorthWest(coord, p)) {
            x--;
            y--;
            Coordinate c = new Coordinate(x, y);
            while (isValidCoordinate(c) && board.getPlayerAt(new Coordinate(x, y)) == getOpponentOf(p)) {
                coords.add(c);
                x--;
                y--;
                c = new Coordinate(x, y);
            }
        }
        return coords;
    }

    private Set<Coordinate> getCoordinatePlayAtSouthWest(Coordinate coord, Player p) {
        int x = coord.getX();
        int y = coord.getY();
        Set<Coordinate> coords = new HashSet<>();
        if (canPlayAtSouthWest(coord, p)) {
            x--;
            y++;
            Coordinate c = new Coordinate(x, y);
            while (isValidCoordinate(c) && board.getPlayerAt(new Coordinate(x, y)) == getOpponentOf(p)) {
                coords.add(c);
                x--;
                y++;
                c = new Coordinate(x, y);
            }
        }
        return coords;
    }

    private Set<Coordinate> getCoordinatePlayAtEast(Coordinate coord, Player p) {
        int x = coord.getX();
        int y = coord.getY();
        Set<Coordinate> coords = new HashSet<>();
        if (canPlayAtEast(coord, p)) {
            x++;
            Coordinate c = new Coordinate(x, y);
            while (isValidCoordinate(c) && board.getPlayerAt(new Coordinate(x, y)) == getOpponentOf(p)) {
                coords.add(c);
                x++;
                c = new Coordinate(x, y);
            }
        }
        return coords;
    }

    private Set<Coordinate> getCoordinatePlayAtNorthEast(Coordinate coord, Player p) {
        int x = coord.getX();
        int y = coord.getY();
        Set<Coordinate> coords = new HashSet<>();
        if (canPlayAtNorthEast(coord, p)) {
            x++;
            y--;
            Coordinate c = new Coordinate(x, y);
            while (isValidCoordinate(c) && board.getPlayerAt(new Coordinate(x, y)) == getOpponentOf(p)) {
                coords.add(c);
                x++;
                y--;
                c = new Coordinate(x, y);
            }
        }
        return coords;
    }

    private Set<Coordinate> getCoordinatePlayAtSouthEast(Coordinate coord, Player p) {
        int x = coord.getX();
        int y = coord.getY();
        Set<Coordinate> coords = new HashSet<>();
        if (canPlayAtSouthEast(coord, p)) {
            x++;
            y++;
            Coordinate c = new Coordinate(x, y);
            while (isValidCoordinate(c) && board.getPlayerAt(new Coordinate(x, y)) == getOpponentOf(p)) {
                coords.add(c);
                x++;
                y++;
                c = new Coordinate(x, y);
            }
        }
        return coords;
    }

    private Set<Coordinate> getCoordinatePlayAtNorth(Coordinate coord, Player p) {
        int x = coord.getX();
        int y = coord.getY();
        Set<Coordinate> coords = new HashSet<>();
        if (canPlayAtNorth(coord, p)) {
            y--;
            Coordinate c = new Coordinate(x, y);
            while (isValidCoordinate(c) && board.getPlayerAt(new Coordinate(x, y)) == getOpponentOf(p)) {
                coords.add(c);
                y--;
                c = new Coordinate(x, y);
            }
        }
        return coords;
    }

    private Set<Coordinate> getCoordinatePlayAtSouth(Coordinate coord, Player p) {
        int x = coord.getX();
        int y = coord.getY();
        Set<Coordinate> coords = new HashSet<>();
        if (canPlayAtSouth(coord, p)) {
            y++;
            Coordinate c = new Coordinate(x, y);
            while (isValidCoordinate(c) && board.getPlayerAt(new Coordinate(x, y)) == getOpponentOf(p)) {
                coords.add(c);
                y++;
                c = new Coordinate(x, y);
            }
        }
        return coords;
    }

    private Boolean canPlayAtWest(Coordinate coord, Player p) {
        int y = coord.getY();
        int x = coord.getX();
        if (x == 0 || board.getPlayerAt(new Coordinate(x - 1, y)) != getOpponentOf(p)) {
            return false;
        }
        x--;
        while (isValidCoordinate(new Coordinate(x, y)) && board.getPlayerAt(new Coordinate(x, y)) == getOpponentOf(p)) {
            x--;
        }
        return isValidCoordinate(new Coordinate(x, y)) && board.getPlayerAt(new Coordinate(x, y)) == p;
    }

    private Boolean canPlayAtNorthWest(Coordinate coord, Player p) {
        int x = coord.getX();
        int y = coord.getY();
        if (x == 0 || y == 0 || board.getPlayerAt(new Coordinate(x - 1, y - 1)) != getOpponentOf(p)) {
            return false;
        }
        x--;
        y--;
        while (isValidCoordinate(new Coordinate(x, y)) && board.getPlayerAt(new Coordinate(x, y)) == getOpponentOf(p)) {
            x--;
            y--;
        }
        return isValidCoordinate(new Coordinate(x, y)) && board.getPlayerAt(new Coordinate(x, y)) == p;
    }


    private Boolean canPlayAtSouthWest(Coordinate coord, Player p) {
        int x = coord.getX();
        int y = coord.getY();
        if (x == 0 || y == Board.HEIGHT - 1 || board.getPlayerAt(new Coordinate(x - 1, y + 1)) != getOpponentOf(p)) {
            return false;
        }
        x--;
        y++;
        while (isValidCoordinate(new Coordinate(x, y)) && board.getPlayerAt(new Coordinate(x, y)) == getOpponentOf(p)) {
            x--;
            y++;
        }
        return isValidCoordinate(new Coordinate(x, y)) && board.getPlayerAt(new Coordinate(x, y)) == p;
    }

    private Boolean canPlayAtEast(Coordinate coord, Player p) {
        int x = coord.getX();
        int y = coord.getY();
        if (x == Board.WIDTH - 1 || board.getPlayerAt(new Coordinate(x + 1, y)) != getOpponentOf(p)) {
            return false;
        }
        x++;
        while (isValidCoordinate(new Coordinate(x, y)) && board.getPlayerAt(new Coordinate(x, y)) == getOpponentOf(p)) {
            x++;
        }
        return isValidCoordinate(new Coordinate(x, y)) && board.getPlayerAt(new Coordinate(x, y)) == p;
    }

    private Boolean canPlayAtNorthEast(Coordinate coord, Player p) {
        int x = coord.getX();
        int y = coord.getY();
        if (x == Board.WIDTH - 1 || y == 0 || board.getPlayerAt(new Coordinate(x + 1, y - 1)) != getOpponentOf(p)) {
            return false;
        }
        x++;
        y--;
        while (isValidCoordinate(new Coordinate(x, y)) && (board.getPlayerAt(new Coordinate(x, y)) == getOpponentOf(p))) {
            x++;
            y--;
        }
        return isValidCoordinate(new Coordinate(x, y)) && board.getPlayerAt(new Coordinate(x, y)) == p;
    }

    private Boolean canPlayAtSouthEast(Coordinate coord, Player p) {
        int x = coord.getX();
        int y = coord.getY();
        if (x == Board.WIDTH - 1 || y == Board.HEIGHT - 1 || board.getPlayerAt(new Coordinate(x + 1, y + 1)) != getOpponentOf(p)) {
            return false;
        }
        x++;
        y++;
        while (isValidCoordinate(new Coordinate(x, y)) && board.getPlayerAt(new Coordinate(x, y)) == getOpponentOf(p)) {
            x++;
            y++;
        }
        return isValidCoordinate(new Coordinate(x, y)) && board.getPlayerAt(new Coordinate(x, y)) == p;
    }

    private Boolean canPlayAtNorth(Coordinate coord, Player p) {
        int x = coord.getX();
        int y = coord.getY();
        if (y == 0 || board.getPlayerAt(new Coordinate(x, y - 1)) != getOpponentOf(p)) {
            return false;
        }
        y--;
        while (isValidCoordinate(new Coordinate(x, y)) && board.getPlayerAt(new Coordinate(x, y)) == getOpponentOf(p)) {
            y--;
        }
        return isValidCoordinate(new Coordinate(x, y)) && board.getPlayerAt(new Coordinate(x, y)) == p;
    }

    private Boolean canPlayAtSouth(Coordinate coord, Player p) {
        int x = coord.getX();
        int y = coord.getY();
        if (y == Board.HEIGHT - 1 || board.getPlayerAt(new Coordinate(x, y + 1)) != getOpponentOf(p)) {
            return false;
        }
        y++;
        while (isValidCoordinate(new Coordinate(x, y)) && board.getPlayerAt(new Coordinate(x, y)) == getOpponentOf(p)) {
            y++;
        }
        return isValidCoordinate(new Coordinate(x, y)) && board.getPlayerAt(new Coordinate(x, y)) == p;
    }


    public boolean isPause() {
        return pause;
    }
}
