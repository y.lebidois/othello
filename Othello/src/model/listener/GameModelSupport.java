package model.listener;

import java.util.LinkedList;
import java.util.List;

public class GameModelSupport {

    // ATTRIBUTS
    private List<GameModelListener> listenerList;


    // CONSTRUCTEURS

    public GameModelSupport() {
        super();
        listenerList = new LinkedList<>();
    }

    // REQUETES

    public synchronized void addBoardListener(GameModelListener listener) {
        if (listener == null) {
            return;
        }
        listenerList.add(listener);
    }

    public synchronized void removeBoardListener(GameModelListener listener) {
        if (listener == null) {
            return;
        }
        listenerList.remove(listener);
    }

    // COMMANDES

    public void fireGameEnded() {
        for (GameModelListener listener : new LinkedList<>(listenerList)) {
            listener.gameEnded();
        }
    }

    public void fireGameChanged() {
        for (GameModelListener listener : new LinkedList<>(listenerList)) {
            listener.gameChanged();
        }
    }

    public void fireGameStarted() {
        for (GameModelListener listener : new LinkedList<>(listenerList)) {
            listener.gameStarted();
        }
    }

}