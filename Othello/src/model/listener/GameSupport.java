package model.listener;

import java.util.LinkedList;
import java.util.List;

public class GameSupport {
    // ATTRIBUTS
    private List<GameListener> listenerList;

    // CONSTRUCTEURS

    public GameSupport() {
        super();
        listenerList = new LinkedList<>();
    }

    // REQUETES

    public synchronized void addGameListener(GameListener listener) {
        if (listener == null) {
            return;
        }
        listenerList.add(listener);
    }

    public synchronized void removeGameListener(GameListener listener) {
        if (listener == null) {
            return;
        }
        listenerList.remove(listener);
    }

    // COMMANDES

    public void fireContentChanged() {
        for (GameListener listener : new LinkedList<>(listenerList)) {
            listener.contentChanged();
        }
    }
}
