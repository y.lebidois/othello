package model.listener;


import java.util.EventListener;

public interface GameModelListener extends EventListener {
    void gameEnded();
    void gameChanged();
    void gameStarted();
}